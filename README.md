You can run the notebooks in this repository interactively at
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/evertrol%2Fsummer-school/master).
